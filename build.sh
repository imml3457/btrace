#!/usr/bin/env bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${script_dir}

if [ -f build.options ]; then
    source ${script_dir}/build.options
fi

# Make sure we have defaults that work.
DEF_CLANG="clang"
DEF_LLVM_STRIP="llvm-strip"
DEF_BCC_SRC_PATH="/home/imulet/all-trace-deps/kernel-depy/bcc"
DEF_LIBBPF_SRC_PATH="${DEF_BCC_SRC_PATH}/src/cc/libbpf/src"
DEF_BPFTOOL="$(realpath ${script_dir}/bpftool)"

CLANG=${CLANG:-${DEF_CLANG}}
LLVM_STRIP=${LLVM_STRIP:-${DEF_LLVM_STRIP}}
BCC_SRC_PATH=${BCC_SRC_PATH:-${DEF_BCC_SRC_PATH}}
LIBBPF_SRC_PATH=${LIBBPF_SRC_PATH:-${BCC_SRC_PATH}/src/cc/libbpf/src}
BPFTOOL=${BPFTOOL:-${DEF_BPFTOOL}}

ARCH=$(uname -m | sed 's/x86_64/x86/' | sed 's/aarch64/arm64/' | sed 's/ppc64le/powerpc/')
# OPT="-O0"
OPT="-O3 -march=native -mtune=native"
LTO="-flto"

if ! [ -z "${LTO}" ]; then
    # can we actually use LTO?
    echo 'int main(){}' > /tmp/llvm_lto_test_$$.c
    if ! ${CLANG} -flto /tmp/llvm_lto_test_$$.c -o /tmp/llvm_test_$$ >/dev/null 2>&1; then
        LTO=""
    fi
fi

CFLAGS="${OPT} ${LTO} -Wall -g -fno-omit-frame-pointer"
BPF_CFLAGS="-g -O3 -Wall"
LDFLAGS="${LTO} -fno-omit-frame-pointer -rdynamic -lelf -lz -lm -lpthread -lpfm -lnuma -lrt"

S=$(realpath src)
B=$(realpath build)
rm    -rf ${B}
mkdir -p  ${B}
BBPF=$(realpath build/libbpf)
mkdir -p ${BBPF}
BOBJ=$(realpath build/obj)
mkdir -p ${BOBJ}
BOBP=$(realpath build/obj/bpf)
mkdir -p ${BOBP}
BOMD=$(realpath build/obj/md)
mkdir -p ${BOMD}
BSKL=$(realpath build/skel)
mkdir -p ${BSKL}
BBIN=$(realpath build/bin)
mkdir -p ${BBIN}

echo "Building libbpf.."
cd ${LIBBPF_SRC_PATH}
make -j BUILD_STATIC_ONLY=1 \
        OBJDIR=${BOBJ}      \
        DESTDIR=${BBPF}     \
        INCLUDEDIR=         \
        LIBDIR=             \
        UAPIDIR=            \
        install || exit $?
cd ${script_dir}

echo "Generating BPF skeleton.."
echo "  CC       BTrace.bpf.o"
${CLANG} -o ${BOBP}/BTrace.bpf.o -c    \
    ${BPF_CFLAGS} -Wno-unknown-attributes -Wno-unused-variable -Wno-unused-function \
    -target bpf                            \
    -D__TARGET_ARCH_${ARCH}                \
    -I${B} -I${BBPF} -I${S}/${ARCH} -I${S} \
    src/bpf/BTrace.bpf.c || exit $?
${LLVM_STRIP} -g ${BOBP}/BTrace.bpf.o || exit $?
${BPFTOOL} gen skeleton ${BOBP}/BTrace.bpf.o > ${BSKL}/BTrace.skel.h || exit $?

echo "Compiling BTrace.."
pids=()
for f in src/*.c; do
    echo "  CC       $(basename ${f} ".c").o"
    ${CLANG} -o ${BOMD}/$(basename ${f} ".c").o \
        -c ${CFLAGS}                            \
        -I${BBPF} -I${BSKL} -I${S}/${ARCH}      \
        -Wno-unknown-attributes                 \
        ${f} &
    pids+=($!)
done

for p in ${pids[@]}; do
    wait $p || exit $?
done

echo "  LD       BTrace"
${CLANG} -o ${BBIN}/BTrace ${LDFLAGS} ${BOMD}/*.o ${BOBJ}/libbpf.a || exit $?

echo "Done."
