#ifndef __BPF_COMMON_H__
#define __BPF_COMMON_H__


#define TRACE_BLOCK_SIZE 5000000
#define TRACE_BLOCK_SIZE_NO_PID 5000000

#define IPI_VECTOR 0xf7
#define IRQ_WORK_VEC 0xf6
#define CALL_VEC 0xfc
#define CALL_SINGLE_VEC 0xfb
#define RESC_VECTOR 0xfd


#define B_CPU_SHIFT 0x00000000ffffffff

typedef struct{
    u64 cpu_pid;
    u64 entry;
    u64 time_start;
    u64 arg;
}traceentry;

typedef struct{
    u32 cnt;
    u64 expiration;
}bookkeeping;

typedef struct{
    u32 pid_filter_on;
    u64 s_pid_filter;
    u32 bpf_pid;
}pid_filter_header;

#define B_TRAP 0x0400
#define B_IRQ  0x0500
#define B_TRAPRET 0x0600
#define B_IRQRET  0x0700
#define B_SYSCALL 0x0800
#define B_SYSRET  0x0A00
#define B_IPI     0x207

#define BTRACE_FILENAME        0x001
#define BTRACE_PIDNAME         0x002
#define BTRACE_METHODNAME      0x003
#define BTRACE_TRAPNAME        0x004
#define BTRACE_INTERRUPTNAME   0x005
#define BTRACE_SYSCALL64NAME   0x008

#define B_PAGEFAULT 14
#define B_MWAIT 0x208
#define B_BOTTOM_HALF 255


/* #define L_ROUTINE_TYPES(X)                                                  \ */
/*         X(SYSCALL_ENTRY, 0x0800)                                                 \ */
/*         X(SYSCALL_EXIT, 0x0A00)                                                  \ */
/*         X(IRQ_HANDLER_ENTRY, 2)                                             \ */
/*         X(IRQ_HANDLER_EXIT, 3)                                              \ */
/*         X(MWAIT, 4)                                                         \ */
/*         X(EXCEPTIONS, 5)                                                    \ */
/*         X(SMP_IPI, 6)                                                       \ */
/*         X(IRQ_SINGLE_CALL_VEC_ENTRY, 7)                                     \ */
/*         X(IRQ_SINGLE_CALL_VEC_EXIT, 8)                                      \ */
/*         X(IRQ_CALL_VEC_ENTRY, 9)                                            \ */
/*         X(IRQ_CALL_VEC_EXIT, 10)                                            \ */
/*         X(IRQ_IPI_ENTRY, 11)                                                \ */
/*         X(IRQ_IPI_EXIT, 12)                                                 \ */
/*         X(IRQ_WORK_ENTRY, 13)                                               \ */
/*         X(IRQ_WORK_EXIT, 14) */
/*  */
/* #define E(t,v) t=v, */
/*  */
/* enum{ L_ROUTINE_TYPES(E) NR_TYPES }; */
/*  */
/* #define S(t,v) case v: return #t; */
/*  */
/* static const char* type_str(u32 t){ */
/*     switch(t){ */
/*         L_ROUTINE_TYPES(S) */
/*     } */
/*     return NULL; */
/* } */

/* enum routine_types{ */
/*     SYSCALL_ENTRY, */
/*     SYSCALL_EXIT, */
/*     IRQ_HANDLER_ENTRY, */
/*     IRQ_HANDLER_EXIT, */
/*     MWAIT, */
/*     EXCEPTIONS, */
/*     SMP_IPI, */
/*     IRQ_SINGLE_CALL_VEC_ENTRY, */
/*     IRQ_SINGLE_CALL_VEC_EXIT, */
/*     IRQ_CALL_VEC_ENTRY, */
/*     IRQ_CALL_VEC_EXIT, */
/*     IRQ_IPI_ENTRY, */
/*     IRQ_IPI_EXIT, */
/*     IRQ_WORK_ENTRY, */
/*     IRQ_WORK_EXIT */
/* }; */
#endif
