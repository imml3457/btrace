#ifndef __BTRACE_MSG_H__
#define __BTRACE_MSG_H__

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include "common.h"

#define BMSG_MQ_NAME "/bmsg.mq"
#define BMSG_PRIO    (0)

enum {
    BMSG_ADD,
    BMSG_DEL,
    BMSG_END,
};


typedef struct __attribute__((packed)){
    uint8_t msg_type;
    u32 pid;
} btrace_msg_header;

typedef struct __attribute__((packed)) {
    u64   exp;
} btrace_add_msg;

typedef struct __attribute__((packed)) {
} btrace_del_msg;

typedef struct __attribute__((packed)) {
} btrace_end_msg;


typedef struct __attribute__((packed)){
    btrace_msg_header header;
    union{
        btrace_add_msg  add;
        btrace_del_msg  del;
        btrace_end_msg  end;
    };
} btrace_msg;


#ifdef BTRACE_MSG_SENDER

typedef struct {
    mqd_t qd;
} btrace_msg_sender;

int bmsg_start_sender(btrace_msg_sender *sender);
int bmsg_close_sender(btrace_msg_sender *sender);
int bmsg_send(btrace_msg_sender *sender, btrace_msg *msg);

#endif /* BTRACE_MSG_SENDER */

#ifdef BTRACE_MSG_RECEIVER

typedef struct {
    mqd_t qd;
} btrace_msg_receiver;

int bmsg_start_receiver(btrace_msg_receiver *receiver);
int bmsg_close_receiver(btrace_msg_receiver *receiver);
int bmsg_receive(btrace_msg_receiver *receiver, btrace_msg *msg);

#endif /* BTRACE_MSG_RECEIVER */


#ifdef BTRACE_MSG_IMPL

static uint32_t bmsg_size(btrace_msg *msg) {
    uint32_t size;

    size = sizeof(btrace_msg_header);

    switch(msg->header.msg_type) {
        case BMSG_ADD:
            size += sizeof(btrace_add_msg);
            break;
        case BMSG_DEL:
            size += sizeof(btrace_del_msg);
            break;
        case BMSG_END:
            size += sizeof(btrace_end_msg);
            break;
        default:
            size = 0;
    }

    return size;
}

#ifdef BTRACE_MSG_SENDER

int bmsg_start_sender(btrace_msg_sender *sender) {
    int err;

    err = 0;

    if ((sender->qd = mq_open(BMSG_MQ_NAME, O_WRONLY)) == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    return err;
}

int bmsg_close_sender(btrace_msg_sender *sender) { return mq_close(sender->qd); }

int bmsg_send(btrace_msg_sender *sender, btrace_msg *msg) {
    int err;

    err = mq_send(sender->qd, (const char*)msg, bmsg_size(msg), BMSG_PRIO);

    if (err == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    return err;
}

#endif /* BTRACE_MSG_SENDER */

#ifdef BTRACE_MSG_RECEIVER

int bmsg_start_receiver(btrace_msg_receiver *receiver) {
    int            err;
    struct mq_attr attr;
    mode_t         old_mode;

    err = 0;

    mq_unlink(BMSG_MQ_NAME);

    attr.mq_flags   = 0;
    attr.mq_maxmsg  = 10;
    attr.mq_msgsize = sizeof(btrace_msg);
    attr.mq_curmsgs = 0;

    old_mode = umask(0);
    if ((receiver->qd = mq_open(BMSG_MQ_NAME, O_RDONLY | O_CREAT | O_EXCL, 0666, &attr)) == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    umask(old_mode);
    return err;
}

int bmsg_close_receiver(btrace_msg_receiver *receiver) {
    int err;

    err = mq_close(receiver->qd);
    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    mq_unlink(BMSG_MQ_NAME);

    return err;
}

int bmsg_receive(btrace_msg_receiver *receiver, btrace_msg *msg) {
    int err;

    err = mq_receive(receiver->qd, (char*)msg, sizeof(*msg), NULL);

    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    return err;
}

#endif /* BTRACE_MSG_RECEIVER */
#endif /* BTRACE_MSG_IMPL */

#endif
