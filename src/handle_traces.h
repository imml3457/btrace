#ifndef __HANDLE_TRACES_H__
#define __HANDLE_TRACES_H__

#include "array.h"

#define ARG0_MASK      0x000000000000ffffl
#define RETVAL_MASK    0x0000000000ff0000l
#define DELTA_MASK     0x00000000ff000000l
#define EVENT_MASK     0x00000fff00000000l
#define TIMESTAMP_MASK 0xfffff00000000000l
#define EVENT_DELTA_RETVAL_MASK (EVENT_MASK | DELTA_MASK | RETVAL_MASK)
#define EVENT_RETURN_BIT           0x0000020000000000l
#define EVENT_LENGTH_FIELD_MASK 0x000000000000000fl

#define UNSHIFTED_RETVAL_MASK 0x00000000000000ffl
#define UNSHIFTED_DELTA_MASK  0x00000000000000ffl
#define UNSHIFTED_EVENT_MASK  0x0000000000000fffl
#define UNSHIFTED_TIMESTAMP_MASK 0x00000000000fffffl
#define UNSHIFTED_EVENT_RETURN_BIT 0x0000000000000200l
#define UNSHIFTED_EVENT_HAS_RETURN_MASK 0x0000000000000c00l

#define MIN_EVENT_WITH_LENGTH 0x010l
#define MAX_EVENT_WITH_LENGTH 0x1ffl
#define MAX_DELTA_VALUE 255
#define MAX_PIDNAME_LENGTH 16

#define RETVAL_SHIFT 16
#define DELTA_SHIFT 24
#define EVENT_SHIFT 32
#define TIMESTAMP_SHIFT 44
#define EVENT_LENGTH_FIELD_SHIFT 4

#define FULL_TIMESTAMP_MASK 0x00ffffffffffffffl
#define CPU_NUMBER_SHIFT 56

#define GETTIMEOFDAY_MASK 0x00ffffffffffffffl
#define FLAGS_SHIFT 56

#define NUMBER_OF_TRACES_IN_BLOCK 8192
#define CYCLE_BUF_SIZE 200000000
#define CPU_MHZ 1400

struct trace_block_64{
    u64 indiv_trace[NUMBER_OF_TRACES_IN_BLOCK];
    u16 trace_idx;
};

extern array_t block_buffer;

struct cycle_data{
    u64 ns;
    u64 ts;
};

typedef struct{
    int number;
    const char* name;
} NumNamePair;

extern u64 start_cycles;

extern u64 end_cycles;

extern struct cycle_data* c_data;
typedef char irqname[64];
extern NumNamePair localirqpairs[256];
extern irqname irqnames[256];

struct trace_block_64* init_trace_block(u32, u64);

void init_block_buffer(int, u64);

u32 find_empty_block(u32);

void fill_block(void*);

u64 get_ts_of_trace(u64);

u64 cycles_to_usec(u64);

void emit_name(const NumNamePair* ipair, u64 event, struct trace_block_64*);
void GetIrqNames(NumNamePair* irqpairs, irqname* irqnames);

#endif
