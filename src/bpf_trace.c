#include "bpf_trace_helpers.h"
#include "bpf_map_helpers.h"
#include "BTrace.skel.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <asm/unistd.h>
#include <sys/mman.h>
#include "bpf_trace.h"
#include <pthread.h>
#include "bpf_common.h"
#include <bpf/libbpf.h>
#include "tree.h"
#include <stdatomic.h>
#include <string.h>
#include "array.h"
#include "handle_traces.h"
#include "kutrace_control_names.h"

#define BTRACE_MSG_RECEIVER
#define BTRACE_MSG_IMPL

#include "btrace_msg.h"

#define BTRACE_IDENTIFIER_STR "bt%u"

int bpf_processes_fd;
int bpf_traces_fd;
int bpf_traces_no_filter_fd;
int bpf_no_pid_header_fd;
int bpf_check_pid_header_fd;
int bpf_trace_ring_buffer;

unsigned long long* index_ptr;
static pthread_t  start_printk;
static pthread_t  get_cycle_thread;
int kill_cycle_thread = 0;

int pid_filter_flag;

pid_filter_header* hdr;

static btrace_msg_receiver  receiver;
static pthread_t            add_message_thread;

tree(u32, m_lock) lock_tree;
tree_it(u32, m_lock) iter;

struct ring_buffer* ring_buffer;

array_t temp_trace_arr_storage;

struct pid_info {
    pid_t p;
    u32* c;
};

atomic_int res;
struct pid_info *pid_to_print;
int pid_iter;

struct cycle_data* c_data;

int c_data_idx = 0;

typedef struct arg_struct{
    btrace_msg *m;
    message_lock* lk;
}args;

char* file_str;

FILE* fp;

void* cycle_thread(){
    u64 first_trace = 0;
    while(kill_cycle_thread == 0){
        u64 ns_clock = measure_time_now_ns();
        u64 ts_rd = rdtsc();
        if(c_data_idx == 0){
            first_trace = ns_clock;
        }
/*         fprintf(stderr, "what is rdtsc: %lu\n", ns_clock); */
        c_data[c_data_idx].ns = ns_clock;
        c_data[c_data_idx].ts = ts_rd;
        c_data_idx++;
        if(c_data_idx == CYCLE_BUF_SIZE){
            fwrite(c_data, sizeof(struct cycle_data), CYCLE_BUF_SIZE, fp);
            c_data_idx = 0;
        }
        usleep(1000);

        if(c_data_idx == 5000){
/*             printf("avg get_time: %lu\n", (ns_clock - first_trace) / c_data_idx); */
        }
    }
    return 0;
}

int handle_trace_ring_buffer_evt(void *ctx, void* data, size_t data_sz){
    traceentry tmp_trace;
    memcpy(&tmp_trace, ((traceentry*)data), sizeof(traceentry));
    array_push(temp_trace_arr_storage, tmp_trace);
    return 0;
}

void close_message_queue(void) {
    bmsg_close_receiver(&receiver);
    INFO("Closed message receiver.\n");
}

int init_messages(void) {
    int err;

    (void)bmsg_size;

    err = bmsg_start_receiver(&receiver);
    if (err != 0) {
        WARN("failed to start, err = %d\n", err);
        goto out;
    }

out:;
    return err;
}
static int open_and_load_BTrace(struct BTrace_bpf **obj) {
        int error;
        struct bpf_map *tmp_trace_map;
        struct bpf_map *temp_ring_buffer_map;
        (*obj) = BTrace_bpf__open();
        if(!(*obj)){
            fprintf(stderr,"failed to open BPF object\n");
        }
        if(pid_filter_flag == 1){
            tmp_trace_map = bpf_object__find_map_by_name((*obj)->obj, "traces");

            if(tmp_trace_map == NULL){
                error = 1;
                goto cleanup;
            }
            int inner_map_buffer_fd = bpf_create_map(BPF_MAP_TYPE_PERCPU_ARRAY, sizeof(u32), sizeof(traceentry), TRACE_BLOCK_SIZE, 0);
            bpf_map__set_inner_map_fd(tmp_trace_map, inner_map_buffer_fd);

            tmp_trace_map = bpf_object__find_map_by_name((*obj)->obj, "t_no_filter");

            if(tmp_trace_map == NULL){
                error = 1;
                goto cleanup;
            }

            inner_map_buffer_fd = bpf_create_map(BPF_MAP_TYPE_PERCPU_ARRAY, sizeof(u32), sizeof(traceentry), 1, 0);
            bpf_map__set_inner_map_fd(tmp_trace_map, inner_map_buffer_fd);
        }
        else{
            tmp_trace_map = bpf_object__find_map_by_name((*obj)->obj, "t_no_filter");

            if(tmp_trace_map == NULL){
                error = 1;
                goto cleanup;
            }

            int inner_map_buffer_fd = bpf_create_map(BPF_MAP_TYPE_PERCPU_ARRAY, sizeof(u32), sizeof(traceentry), TRACE_BLOCK_SIZE_NO_PID, 0);
            bpf_map__set_inner_map_fd(tmp_trace_map, inner_map_buffer_fd);

            tmp_trace_map = bpf_object__find_map_by_name((*obj)->obj, "traces");

            if(tmp_trace_map == NULL){
                error = 1;
                goto cleanup;
            }

            inner_map_buffer_fd = bpf_create_map(BPF_MAP_TYPE_PERCPU_ARRAY, sizeof(u32), sizeof(traceentry), 1, 0);
            bpf_map__set_inner_map_fd(tmp_trace_map, inner_map_buffer_fd);


        }

        error = BTrace_bpf__load((*obj));
        if(error) {
            fprintf(stderr,"failed to load BPF object: %d\n", error);
            goto cleanup;
        }

        temp_ring_buffer_map = bpf_object__find_map_by_name((*obj)->obj, "trace_rb");
        if(tmp_trace_map == NULL){
            error = 1;
            goto cleanup;
        }
        ring_buffer = ring_buffer__new(bpf_map__fd(temp_ring_buffer_map), handle_trace_ring_buffer_evt, NULL, NULL);
        if(ring_buffer == NULL){
            fprintf(stderr, "failed to make new ringbuffer like the thing after finding the fd\n");
            error = 1;
            goto cleanup;
        }

        error = BTrace_bpf__attach((*obj));
        if(error){
            fprintf(stderr,"failed to attach BPF object: %d\n", error);
            goto cleanup;
        }
        return 0;

cleanup:
    BTrace_bpf__destroy((*obj));
    (*obj) = NULL;
    return error != 0;
}


int init_bpf_trace(void) {
    struct BTrace_bpf *obj;
    int error;

    error = bump_memlock_rlimit();
    if(error){
        fprintf(stderr,"failed to increase rlimit\n");
        return error;
    }
    if(pid_filter_flag == 1){
        error = open_and_load_BTrace(&obj);
        if(error){
            fprintf(stderr,"failed to open and load BPF object: %d\n", error);
            return error;
        }
        bpf_processes_fd = map_fd_by_name("processes");
        if(bpf_processes_fd == -1){
            goto cleanup;
        }
        bpf_traces_fd = map_fd_by_name("traces");
        if(bpf_traces_fd == -1){
            fprintf(stderr, "failed to open traces map\n");
            goto cleanup;
        }
        bpf_check_pid_header_fd = map_fd_by_name("pid_filter");
        if(bpf_check_pid_header_fd == -1){
            goto cleanup;
        }
    }
    //no pid filtering
    //do ring buffer here
    else{
        error = open_and_load_BTrace(&obj);
        if(error){
            fprintf(stderr,"failed to open and load BPF object: %d\n", error);
            return error;
        }
        bpf_traces_no_filter_fd = map_fd_by_name("t_no_filter");
        if(bpf_traces_no_filter_fd == -1){
            fprintf(stderr, "failed to open traces map\n");
            goto cleanup;
        }
        bpf_check_pid_header_fd = map_fd_by_name("pid_filter");
        if(bpf_check_pid_header_fd == -1){
            goto cleanup;
        }
    }

    return 0;

cleanup:
    BTrace_bpf__destroy(obj);
    obj = NULL;
    return error != 0;
}



static void* start_pk(void* arg){
    // for debugging with bpf_printk
    printf("starting print server\n");
    FILE *trace_pipe;
    int trace_fd, flags;
    char line[512];

    trace_pipe = fopen("/sys/kernel/debug/tracing/trace_pipe", "r");
    trace_fd = fileno(trace_pipe);
    flags = fcntl(trace_fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(trace_fd, F_SETFL, flags);

    for(;;){
        while (fgets(line, sizeof(line), trace_pipe)) {
            fprintf(stderr, "%s", line);
        }
        fflush(stdout);
        sleep(1);
    }
}

void add_to_bpf(u32 pid, u64 exp){
    u64 e = 0;
    int nr_cpus = libbpf_num_possible_cpus();
    printf("number of cpus %d\n", nr_cpus);
    bookkeeping* T = malloc(nr_cpus * sizeof(bookkeeping));
    int i;
    if(exp == 0){
        e = 0;
    }
    else{
        e = measure_time_now_ns() + exp;
    }
    for(i = 0; i < nr_cpus; i++){
        T[i].cnt = 0;
        T[i].expiration = e;
    }
    int status = bpf_map_update_elem(bpf_processes_fd, &pid, T, 0);
    char buf[16];
    sprintf(buf, BTRACE_IDENTIFIER_STR, pid);
    int buffer_fd = bpf_create_map_name(BPF_MAP_TYPE_PERCPU_ARRAY, "temp", sizeof(u32), sizeof(traceentry), TRACE_BLOCK_SIZE, 0);
    bpf_map_update_elem(bpf_traces_fd, &pid, &buffer_fd, 0);
    if(status != 0){
        ERR("bpf map update elem failed with status %d\n", status);
    }
    close(buffer_fd);
}

static void* get_add_message(void* _arg){
    args* arg = _arg;
    //pass arg to msg variable

    add_to_bpf(arg->m->header.pid, arg->m->add.exp);
    pthread_mutex_lock(&(arg->lk->mutex));

    while(arg->lk->alive){
        pthread_cond_wait(&(arg->lk->cnd), &(arg->lk->mutex));
    }

    int nr_cpus = libbpf_num_possible_cpus();

    (pid_to_print[pid_iter]).p = arg->m->header.pid;

    bookkeeping* tmp = malloc(nr_cpus * sizeof(bookkeeping));
    bpf_map_lookup_elem(bpf_processes_fd, &(arg->m->header.pid), tmp);
    (pid_to_print[pid_iter]).c = malloc(nr_cpus * sizeof(u32));
    for(int i = 0; i < nr_cpus; i++){
        (pid_to_print[pid_iter]).c[i] = tmp[i].cnt;
    }
    pid_iter++;

    bpf_map_delete_elem(bpf_processes_fd, &(arg->m->header.pid));
    //unlock the thread
    free(arg->lk);
    free(arg->m);
    free(arg);

    res--;

    pthread_mutex_unlock(&(arg->lk->mutex));
    return NULL;
}

void run_pid_filter_btrace(){
    int zero = 0;
    int err = init_bpf_trace();
    if(err){
        ERR("error occured in main() errno: %d\n", err);
    }
    int status = pthread_create(&start_printk, NULL, &start_pk, NULL);
    if (status)
        ERR("sussy\n");

    status = bpf_map_update_elem(bpf_check_pid_header_fd, &zero, &hdr, 0);

    lock_tree = tree_make(u32, m_lock);
    res = 0;
    pid_to_print = malloc(sizeof(struct pid_info) * 10000);
    pid_iter = 0;
    err = init_messages();

    for(;;){
        btrace_msg *msg = malloc(sizeof(btrace_msg));
/*         fprintf(stderr, "about to get a message or halt\n"); */
        err = bmsg_receive(&receiver, msg);
        fprintf(stderr, "recieved a message!\n");
        if (err <= 0){
            ERR("failed to recieve message from message sender: errno = %d\n", err);
        }

        message_lock* add_lock = malloc(sizeof(message_lock));
        pthread_mutex_init(&(add_lock->mutex), NULL);
        pthread_cond_init(&(add_lock->cnd), NULL);
        add_lock->alive = 1;
        switch (msg->header.msg_type){
            case BMSG_ADD:
                tree_insert(lock_tree, msg->header.pid, add_lock);
                args *arg = malloc(sizeof(args));
                arg->m = msg;
                arg->lk = add_lock;
                res++;
                int status = pthread_create(&add_message_thread, NULL, &get_add_message, arg);
                if (status < 0){
                    ERR("failed to create thread: errno %d\n", status);
                }
                break;
            case BMSG_DEL:
                printf("inside the delete\n");
                iter = tree_lookup(lock_tree, msg->header.pid);
                if(tree_it_good(iter)){
                    pthread_mutex_lock(&(tree_it_val(iter)->mutex));
                    tree_it_val(iter)->alive = 0;
                    pthread_cond_signal(&(tree_it_val(iter)->cnd));
                    pthread_mutex_unlock(&(tree_it_val(iter)->mutex));
                }
                else{
                    ERR("delete failed homie\n");
                }
                break;
            case BMSG_END:
                goto print;
                break;
            }
        }
    print:;
        int nr_cpus = libbpf_num_possible_cpus();
        int all_zero = 0;
        while(res) {}
        traceentry* traces = malloc(nr_cpus * sizeof(traceentry));
        for(int i = 0; i < pid_iter; i++){
            int tmp_fd;
            u32 bt_id = 0;
            bpf_map_lookup_elem(bpf_traces_fd, &pid_to_print[i].p, &bt_id);
            for(int j = 0; j < TRACE_BLOCK_SIZE; j++){
                tmp_fd = bpf_map_get_fd_by_id(bt_id);
                bpf_map_lookup_elem(tmp_fd, &j, traces);
                for (int z = 0; z < nr_cpus; z++){
                    if(traces[z].entry == 0 && traces[z].cpu_pid == 0){
                        all_zero++;
                    }
                    if(traces[z].cpu_pid != 0){
/*                         printf("type: %lu pid: %d entry: %ld cpu: %lu\n", traces[z].type, pid_to_print[i].p, traces[z].entry, traces[z].cpu_pid); */
                    }
                }
                if(all_zero == nr_cpus){
                    break;
                }
                else{
                    all_zero = 0;
                }
            }
            iter = tree_lookup(lock_tree, pid_to_print[i].p);
            if(tree_it_good(iter)){
                free(tree_it_val(iter));
            }
        }
        free(traces);
        free(pid_to_print);
        tree_free(lock_tree);
}

/*
    Main function for obtaining the traces from kernel
    and parsing them into a raw binary

*/

void run_btrace_all(u64 dur){
    temp_trace_arr_storage = array_make(traceentry);
    int zero = 0;
    int status = pthread_create(&start_printk, NULL, &start_pk, NULL);
    if (status) ERR("sussy baka >_< uWu - Austin Rhodes\n");

/*     status = pthread_create(&get_cycle_thread, NULL, &cycle_thread, NULL); */
/*     if (status) ERR("sussy baka >_< uWu - Austin Rhodes\n"); */


    /* Main call to init bpf */
    int err = init_bpf_trace();
    if(err) ERR("error occured in main() errno: %d\n", err);

    /* Filter the pid of bpf (might not be needed) */
    hdr->bpf_pid = getpid();
    hdr->s_pid_filter = 1;

    /* updates flag to signal bpf to start writing to ring_buf */
    status = bpf_map_update_elem(bpf_check_pid_header_fd, &zero, hdr, 0);

    /* Sleep while bpf is running */
    u64 nano_dur = dur / 1000;
    usleep(nano_dur);

    /* Signal bpf to stop writing to ring_buf */
    hdr->s_pid_filter = 0;
    status = bpf_map_update_elem(bpf_check_pid_header_fd, &zero, hdr, 0);

    /*
        Poll the ring buffer and arrange data
        Ideally this is called once and the entire ring_buffer is consumed
        Since we plan to stop tracing after this
    */

    ring_buffer__poll(ring_buffer, 0);

    /* Start parsing the trace data */
    int first_iter = 0;
    traceentry* trace_ptr;
    traceentry* trace_ptr_end;

    trace_ptr_end = array_last(temp_trace_arr_storage);
    end_cycles = trace_ptr_end->time_start;

    trace_ptr_end = array_item(temp_trace_arr_storage, 0);
    start_cycles = trace_ptr_end->time_start;
    FILE* f;
    f = fopen(file_str, "wb");
    GetIrqNames(localirqpairs, irqnames);

    array_traverse(temp_trace_arr_storage, trace_ptr){
        if(first_iter == 0){
            init_block_buffer(trace_ptr->cpu_pid & 0x00000000ffffffff, trace_ptr->time_start);
            first_iter = 1;
        }
/*         printf("entry: %ld cpu: %lu pid: %lu timestamp: %lu\n", trace_ptr->entry, trace_ptr->cpu_pid & 0x00000000ffffffff, (trace_ptr->cpu_pid >> 32), trace_ptr->time_start); */
        fill_block((void*)trace_ptr);
    }

    if(f == NULL) ERR("ERROR: failed to open file errno: %d FILE NAME: %s\n", err, file_str);


    /* Write to the binary KUtrace eqv of DoDump */
    struct trace_block_64** block_ptr;
    array_traverse(block_buffer, block_ptr){
        struct trace_block_64* def_block = *(block_ptr);
        fwrite(def_block->indiv_trace, sizeof(u64), NUMBER_OF_TRACES_IN_BLOCK, f);
    }

    fclose(f);
}

int main(int argc, char** argv){

    char* arg_pid_filter = "--pidfilter";
    u64 dur = 0;
    pid_filter_flag = 0;
    char *end_ptr;
    temp_trace_arr_storage = array_make(traceentry*);
    hdr = malloc(sizeof(pid_filter_header));
    c_data = malloc(sizeof(struct cycle_data) * CYCLE_BUF_SIZE);
/*     fp = fopen("rdtsc.timings", "wb"); */

    if(argc >= 2){
        if(strcmp(arg_pid_filter, argv[1]) == 0){
            pid_filter_flag = 1;
        }
        else{
            dur = strtoll(argv[1], &end_ptr, 10);
            file_str = argv[2];
        }
    }

    hdr->pid_filter_on = pid_filter_flag;
    hdr->s_pid_filter = 0;


    if(pid_filter_flag == 1){
        run_pid_filter_btrace();
    }
    else{
        run_btrace_all(dur);
    }

    return 1;
}
