#ifndef __BPF_TRACE_H__
#define __BPF_TRACE_H__
#include "tree.h"
#include "common.h"


typedef struct{
    pthread_mutex_t mutex;
    pthread_cond_t cnd;
    int alive;
}message_lock;

extern int bpf_processes_fd;
int init_messages(void);
void close_message_queue(void);
void message_receive(void);

typedef message_lock* m_lock;
use_tree(u32, m_lock);


#endif
