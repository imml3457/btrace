#include <vmlinux.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include <asm-generic/errno.h>
#include "bpf_common.h"

#define BPF_PRINTK 1

#if BPF_PRINTK
#define PRINTK(...) (bpf_printk(__VA_ARGS__))
#else
#define PRINTK(...) ;
#endif

struct syscall_exit_args{
    unsigned long long common_tp_fields;
    long syscall_nr;
    long ret;
};
struct syscall_entry_args{
    unsigned long long common_tp_fields;
    long syscall_nr;
    unsigned long regs[6];
};

struct irq_entry_args{
    unsigned long long common_tp_fields;
    long irq;
};
struct irq_exit_args{
    unsigned long long common_tp_fields;
    long irq;
    long ret;
};

struct fault_entry_args{
    unsigned long long common_tp_fields;
};
struct fault_exit_args{
    unsigned long long common_tp_fields;
};
struct softirq_entry_args{
    unsigned long long common_tp_fields;
    unsigned int vect;
};

struct bpf_map_def SEC("maps") processes = {
    .type = BPF_MAP_TYPE_PERCPU_HASH,
    .key_size = sizeof(u32),
    .value_size = sizeof(bookkeeping),
    .max_entries = 100000,
};


struct bpf_map_def SEC("maps") traces = {
    .type = BPF_MAP_TYPE_HASH_OF_MAPS,
    .key_size = sizeof(u32),
    .value_size = sizeof(u32),
    .max_entries = 10000,
    .map_flags = 0
};

struct bpf_map_def SEC("maps") t_no_filter = {
    .type = BPF_MAP_TYPE_HASH_OF_MAPS,
    .key_size = sizeof(u32),
    .value_size = sizeof(u32),
    .max_entries = 1,
    .map_flags = 0
};

struct bpf_map_def SEC("maps") pid_filter = {
    .type = BPF_MAP_TYPE_HASH,
    .key_size = sizeof(u32),
    .value_size = sizeof(pid_filter_header),
    .max_entries = 1,
    .map_flags = 0
};

struct bpf_map_def SEC("maps") trace_rb = {
    .type = BPF_MAP_TYPE_RINGBUF,
    .max_entries = sizeof(traceentry) * 1 << 25 //this is 4 GB
};

static int should_add_to_buffer(){
    u64 pid_tgid;
    u32 pid;
    bookkeeping *proc;
    pid_filter_header *header;
    u32 *inner_fd;
    u64 *t;
    u32 *cur_cnt;
    u32 zero = 0;
    pid_tgid = bpf_get_current_pid_tgid();
    pid = ( ( pid_tgid >> 32) );


    header = bpf_map_lookup_elem(&pid_filter, &zero);
    if(header == NULL){
        return 0;
    }

    if(header->pid_filter_on == 0){
        if(header->bpf_pid == pid){
            return 0;
        }
        if(header->s_pid_filter == 1){
            return 1;
        }
    }
    else{
        return -1;
    }
    return 0;
}

static void add_trace_pid_filter_on(u64 trace_ent, u64 type){
    u64 pid_tgid;
    u64 p = 0;
    u32 c = 0;
    u32 pid;
    bookkeeping *proc;
    pid_filter_header *header;
    u32 *inner_fd;
    u64 *t;
    u32 *cur_cnt;
    u32 zero = 0;
    pid_tgid = bpf_get_current_pid_tgid();
    pid = ( ( pid_tgid >> 32) );

    proc = bpf_map_lookup_elem(&processes, &pid);
    if(proc == NULL){
        return;
    }
    else{
        inner_fd = bpf_map_lookup_elem(&traces, &pid);
        if(inner_fd == NULL){
            return;
        }
        else{
            p = proc->expiration;
            if(p > bpf_ktime_get_ns() || p == 0){
                c = proc->cnt;
                if(c < TRACE_BLOCK_SIZE){
                    traceentry tmp;
                    tmp.time_start = bpf_ktime_get_ns();
                    tmp.entry = trace_ent;
                    tmp.cpu_pid = (bpf_get_smp_processor_id() + 1);
                    tmp.arg = type;
                    bpf_map_update_elem(inner_fd, &c, &tmp, BPF_ANY);
                    __sync_fetch_and_add(&(proc->cnt), 1);
                }
            }
        }
    }
}

static void add_trace_pid_filter_off(u64 arg, u64 trace_type){
    u64 pid_tgid;
    u64 p = 0;
    u32 c = 0;
    u32 pid;
    bookkeeping *proc;
    pid_filter_header *header;
    u32 *inner_fd;
    u64 *t;
    u32 *cur_cnt;
    u32 zero = 0;
    pid_tgid = bpf_get_current_pid_tgid();
    pid = ( ( pid_tgid >> 32) );

    traceentry tmp;
    __builtin_memset(&tmp, 0, sizeof(tmp));
    tmp.time_start = bpf_ktime_get_ns();
    tmp.entry = trace_type;
    tmp.cpu_pid = (bpf_get_smp_processor_id() + 1);
    u64 tmp_pid = ((u64)pid) << 32;
    tmp.cpu_pid |= tmp_pid;
    tmp.arg = arg;
    bpf_ringbuf_output(&trace_rb, &tmp, sizeof(traceentry), 0);
}

SEC("tp/raw_syscalls/sys_enter")
int sys_enter(struct syscall_entry_args* arg){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(arg->syscall_nr, B_SYSCALL);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(arg->regs[0] & 0xFFFFul, B_SYSCALL | arg->syscall_nr);
    }
    return 0;
}

SEC("tp/raw_syscalls/sys_exit")
int sys_exit(struct syscall_exit_args* arg){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(arg->syscall_nr, B_SYSRET);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(arg->ret & 0xFFFFul, B_SYSRET | arg->syscall_nr);
    }
    return 0;
}

SEC("tp/irq/irq_handler_entry")
int irq_handler_entry(struct irq_entry_args* arg){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(arg->irq & 0xff,B_IRQ);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(arg->irq & 0xff, B_IRQ);
    }
    return 0;
}

SEC("tp/irq/irq_handler_exit")
int irq_handler_exit(struct irq_exit_args* arg){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(arg->irq & 0xff, B_IRQRET);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(arg->irq & 0xff, B_IRQRET);
    }
    return 0;
}

SEC("kprobe/__sysvec_x86_platform_ipi")
int BPF_PROG(__sysvec_x86_platform_ipi_entry){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQ + IPI_VECTOR);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQ + IPI_VECTOR);
    }
    return 0;
}

SEC("kprobe/__sysvec_x86_platform_ipi")
int BPF_PROG(__sysvec_x86_platform_ipi_exit){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQ + IPI_VECTOR);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQ + IPI_VECTOR);
    }
    return 0;
}
SEC("kprobe/__sysvec_irq_work")
int BPF_PROG(__sysvec_irq_work_entry){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQ + IRQ_WORK_VEC);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQ + IRQ_WORK_VEC);
    }
    return 0;
}
SEC("kprobe/__sysvec_irq_work")
int BPF_PROG(__sysvec_irq_work_exit){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQRET + IRQ_WORK_VEC);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQRET + IRQ_WORK_VEC);
    }
    return 0;
}

SEC("kprobe/native_smp_send_reschedule")
int BPF_PROG(native_smp_send_reschedule_entry, int cpu){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(cpu, B_IPI);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(cpu, B_IPI);
    }
    return 0;
}

SEC("kprobe/native_send_call_func_single_ipi")
int BPF_PROG(native_send_call_func_single_ipi_entry, int cpu){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(cpu, B_IPI);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(cpu, B_IPI);
    }
    return 0;
}
SEC("kprobe/native_send_call_func_ipi")
int BPF_PROG(native_send_call_func_ipi_entry){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IPI);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IPI);
    }
    return 0;
}
SEC("kprobe/__sysvec_call_function_single")
int BPF_PROG(__sysvec_call_function_single_entry){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQ + CALL_SINGLE_VEC);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQ + CALL_SINGLE_VEC);
    }
    return 0;
}
SEC("kprobe/__sysvec_call_function_single")
int BPF_PROG(__sysvec_call_function_single_exit){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQRET + CALL_SINGLE_VEC);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQRET + CALL_SINGLE_VEC);
    }
    return 0;
}

SEC("kprobe/__sysvec_call_function")
int BPF_PROG(__sysvec_call_function_entry){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQ + CALL_VEC);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQ + CALL_VEC);
    }
    return 0;
}
SEC("kprobe/__sysvec_call_function")
int BPF_PROG(__sysvec_call_function_exit){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_IRQRET + CALL_VEC);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_IRQRET + CALL_VEC);
    }
    return 0;
}

SEC("tp/exceptions/page_fault_user")
int page_fault_user(struct fault_entry_args* args){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(0, B_TRAP + B_PAGEFAULT);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(0, B_TRAP + B_PAGEFAULT);
    }
    return 0;
}
SEC("kprobe/acpi_idle_do_entry")
int BPF_PROG(acpi_idle_do_entry_entry){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(255, B_MWAIT);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(255, B_MWAIT);
    }
    return 0;
}
SEC("tp/irq/softirq_entry")
int softirq_entry(struct softirq_entry_args* args){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(args->vect, B_IRQ + B_BOTTOM_HALF);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(args->vect, B_IRQ + B_BOTTOM_HALF);
    }
    return 0;
}
SEC("tp/irq/softirq_exit")
int softirq_exit(struct softirq_entry_args* args){
    if(should_add_to_buffer() == -1){
        add_trace_pid_filter_on(args->vect, B_IRQRET + B_BOTTOM_HALF);
    }
    else if(should_add_to_buffer() != 0){
        add_trace_pid_filter_off(args->vect, B_IRQRET + B_BOTTOM_HALF);
    }
    return 0;
}

char LICENSE[] SEC("license") = "GPL";
