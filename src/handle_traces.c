#include "handle_traces.h"
#include "bpf_common.h"
#include "kutrace_control_names.h"
#include <stdlib.h>
#include <stdio.h>

#define KUTRACE_VERSION_POST 3
#define VERSION_MASK 0x0Ful

NumNamePair localirqpairs[256];
irqname irqnames[256];

u64 start_cycles;
u64 end_cycles;
int first_block = 0;
array_t block_buffer;

/*
    Converted from KUtrace 5.10
    This will fit the convention that I am using
    To dump the trace file
*/
void StripCRLF(char* s) {
  int len = strlen(s);
  if ((0 < len) && s[len - 1] == '\n') {s[len - 1] = '\0'; --len;}
  if ((0 < len) && s[len - 1] == '\r') {s[len - 1] = '\0'; --len;}
}

void GetModelName(char* modelname, int len) {
  modelname[0] = '\0';
  FILE *cpuinfo = fopen("/proc/cpuinfo", "rb");
  if (cpuinfo == NULL) {return;}
  char *arg = NULL;
  size_t size = 0;
  // Expecting something like
  // model name	: ARMv7 Processor rev 3 (v7l)
  while(getline(&arg, &size, cpuinfo) != -1)
  {
    if(memcmp(arg, "model name", 10) == 0) {
      const char* colon = strchr(arg, ':');
      if (colon != NULL) {	// Skip the colon and the next space
        StripCRLF(arg);
        strncpy(modelname, colon + 2, len);
        modelname[len - 1] = '\0';
        break;			// Just the first one, then get out
      }
    }
  }
  free(arg);
  fclose(cpuinfo);
  StripCRLF(modelname);
}

// Get next interrupt description line from file, if any, and set
// interrupt number and name and return value of true.
// If no more lines, return false
//
// Expecting:
// cat /proc/interrupts
//            CPU0       CPU1
//   0:         20          0   IO-APIC   2-edge      timer
//   1:          3          0   IO-APIC   1-edge      i8042
//   8:          1          0   IO-APIC   8-edge      rtc0
int NextIntr(FILE* intrfile, int* intrnum, char* intrname, int len) {
  char buffer[256];
    while (fgets(buffer, 256, intrfile)) {
        StripCRLF(buffer);
        int n = sscanf(buffer, "%d:", intrnum);
        if (n != 1) {continue;}			// No intr on this line
        const char* space = strrchr(buffer, ' ');	// NOTE: reverse search
        if (space == NULL) {continue;}		// No name on this line
        if (space[1] == '\0') {continue;}		// Empty name on this line
        strncpy(intrname, space + 1, len);
        intrname[len - 1] = '\0';
        return 1;
    }

    return 0;
}

// Read up to 255 active IRQ names from the running system
void GetIrqNames(NumNamePair* irqpairs, irqname* irqnames) {
    irqpairs[0].number = -1;	// Default end marker
    irqpairs[0].name = NULL;
    FILE* intrfile = fopen("/proc/interrupts", "r");
    if (intrfile == NULL) {return;}

    char intrname[64];
    int intrnum;
    int k = 0;
    while (NextIntr(intrfile, &intrnum, intrname, 64)) {
        memcpy(irqnames[k], intrname, 64);	// Make a copy
        irqpairs[k].number = intrnum;
        irqpairs[k].name = &irqnames[k][0];
        ++k;
        if (255 <= k) {break;}	// Leaving room for NULL end-marker
    }
    fclose(intrfile);
    irqpairs[k].number = -1;	// End marker
    irqpairs[k].name = NULL;
}

void InsertVariableEntry(const char* str, u64 event, u64 arg, struct trace_block_64* blk) {
    u64 temp[8];		// Up to 56 bytes
    u64 bytelen = strlen(str);
    if (bytelen == 0) {return;}		// Skip empty strings
    if (bytelen > 56) {bytelen = 56;}	// If too long, truncate
    u64 wordlen = 1 + ((bytelen + 7) / 8);
    // Build the initial word
    u64 event_with_length = event + (wordlen * 16);
    //         T               N                           ARG
    temp[0] = (0lu << 44) | (event_with_length << 32) | arg;
    memset(&temp[1], 0, 7 * sizeof(u64));
    memcpy((char*)&temp[1], str, bytelen);

    memcpy(&(blk->indiv_trace[blk->trace_idx]), &temp, 64);
    blk->trace_idx += 8;
}

void emit_name(const NumNamePair* ipair, u64 event, struct trace_block_64* blk){
    const NumNamePair* temp = ipair;
    while(temp->name != NULL){
        InsertVariableEntry(temp->name, event, temp->number, blk);
        temp++;
    }
}

/* end KUTrace functions */

/*
    Largely adapted from KUtrace, this initializes the first block
    furthermore, it also emits the names of the IRQs/Pids/Syscalls/Traps

    This also handles creating a new block if desired

*/



struct trace_block_64* init_trace_block(u32 cpu, u64 ts){
    struct trace_block_64 *blk = malloc(sizeof(struct trace_block_64));
    u64 tmp_cpu = (u64)cpu;

    blk->indiv_trace[0] = (ts >> 6 & FULL_TIMESTAMP_MASK) | (tmp_cpu << CPU_NUMBER_SHIFT); //need to get rdtsc
    blk->indiv_trace[1] = 0lu;

    if(first_block == 0){
        blk->indiv_trace[1] |= ((KUTRACE_VERSION_POST & VERSION_MASK) << 56);
        blk->indiv_trace[2] = start_cycles >> 6;
        blk->indiv_trace[3] = start_cycles;
        blk->indiv_trace[4] = end_cycles >> 6;
        blk->indiv_trace[5] = end_cycles;
        blk->indiv_trace[6] = 0lu;
        blk->indiv_trace[7] = 0lu;
        blk->indiv_trace[8] = 99999lu; //pid number
        blk->indiv_trace[9] = 0lu;
        blk->indiv_trace[10] = 1000000lu; //memcpy pid name in the future
        blk->trace_idx = 11;
        emit_name(PidNames, BTRACE_PIDNAME, blk);
        emit_name(TrapNames, BTRACE_TRAPNAME, blk);
        emit_name(IrqNames, BTRACE_INTERRUPTNAME, blk);
        emit_name(localirqpairs, BTRACE_INTERRUPTNAME, blk);
        emit_name(Syscall64Names, BTRACE_SYSCALL64NAME, blk);
    }
    else{
        blk->indiv_trace[2] = 99999lu;
        blk->indiv_trace[3] = 0lu;
        blk->indiv_trace[4] = 1000000lu; //memcpy pid name
        blk->trace_idx = 5;
    }

    blk->indiv_trace[1] |= (ts & 0x00ffffffffffffffl);

    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 8] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 7] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 6] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 5] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 4] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 3] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 2] = 0lu;
    blk->indiv_trace[NUMBER_OF_TRACES_IN_BLOCK - 1] = 0lu;
    return blk;
}

#if 0
u64 get_ts_of_trace(u64 t){
    u64 tmp_ts_idx = (t - c_data[0].ns) / 1000000;
/*     printf("what is arr 1 - 0: %lu\n", c_data[1].ns - c_data[0].ns); */
    if(tmp_ts_idx <= CYCLE_BUF_SIZE){
        return tmp_ts_idx;
    }
    else{
        return 0;
    }
}

#endif



/* Init the first block and the entire block buffer */

void init_block_buffer(int cpu, u64 ts){
    block_buffer = array_make(struct trace_block_64*);
    struct trace_block_64 *fst_block = init_trace_block(cpu, ts);
    array_push(block_buffer, fst_block);
    first_block = 1;
}

/* block addition helper function */

struct trace_block_64* add_new_block(int cpu, u64 ts){
    struct trace_block_64 *new_block = init_trace_block(cpu, ts);
    array_push(block_buffer, new_block);
    return new_block;
}


/*
Fill block obviously handles filling the individual blocks
in the block_buffer. It also handles arranging the
conversion from my traceentry data type to a data type that
KUtrace can understand (u64), lastly it fill the block.

Note: if there is a block that is full or a cpu that does not match a block
it will create a new block and push.

Bug: cpus in event data look wrong, check here and when the block is init'd
*/

void fill_block(void* trace_to_emplace){
    u64 trace_64 = 0;
    u64 arg = 0;
    traceentry* trace_tmp = (traceentry*)trace_to_emplace;
    arg |= ((trace_tmp->entry << EVENT_SHIFT) | trace_tmp->arg);
    trace_64 = arg | ((trace_tmp->time_start >> 6) << TIMESTAMP_SHIFT);
    struct trace_block_64** block_ptr;
    array_traverse(block_buffer, block_ptr){
        struct trace_block_64* def_block = *(block_ptr);
        u64 which_cpu = def_block->indiv_trace[0] >> CPU_NUMBER_SHIFT;
        if(which_cpu == (trace_tmp->cpu_pid & B_CPU_SHIFT) && def_block->trace_idx != NUMBER_OF_TRACES_IN_BLOCK){
            def_block->indiv_trace[def_block->trace_idx] = trace_64;
            def_block->trace_idx += 1;
            return;
        }
    }

    //did not find a block
    struct trace_block_64* tmp_add_block;
    tmp_add_block = add_new_block(trace_tmp->cpu_pid & B_CPU_SHIFT, trace_tmp->time_start);
    tmp_add_block->indiv_trace[tmp_add_block->trace_idx] = trace_64;
    tmp_add_block->trace_idx += 1;
}
