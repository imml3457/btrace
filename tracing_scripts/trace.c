#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <signal.h>
#define BTRACE_MSG_IMPL
#define BTRACE_MSG_SENDER
#include "src/btrace_msg.h"

pid_t childPID;

void kill_child(int signum){
    kill(childPID, SIGKILL);
}

int main(int argc, char** argv, char* const* envp){
    btrace_msg_sender sender;
    bmsg_start_sender(&sender);
    btrace_msg msg;
    childPID = fork();
    if(childPID == 0){
        execvp(argv[1], argv + 1);
        printf("exec failed\n");
        exit(1);
    }

    else if(childPID < 0){
        fprintf(stderr, "fork failed\n");
    }

    else{
        int return_status;
        signal(SIGINT, kill_child);
        msg.header.pid = childPID;
        msg.header.msg_type = 0;
        msg.add.exp = 0;

        bmsg_send(&sender, &msg);
        wait4(childPID, &return_status, 0, NULL);
        msg.header.msg_type = 1;
        bmsg_send(&sender, &msg);
        msg.header.msg_type = 2;
        bmsg_send(&sender, &msg);
        printf("return status %d\n", return_status);
    }
   return 0;
}
